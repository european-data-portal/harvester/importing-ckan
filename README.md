# importing ckan
Microservice for importing from source and feeding a pipe.

The service is based on the [pipe-connector](https://gitlab.com/european-data-portal/harvester/pipe-connector) library. Any configuration applicable for the pipe-connector can also be used for this service.

## Table of Contents
1. [Pipe Configuration](#pipe-configuration)
1. [Data Info Object](#data-info-object)
1. [Build](#build)
1. [Run](#run)
1. [Docker](#docker)
1. [License](#license)

## Pipe Configuration

_mandatory_

* `address` 

    Address of the source

_optional_

* `lastRun` 

    Date and time anchor for incremental importing.

* `incremental` 

    Requires `lastRun`. Imports only changes after `lastRun`

* `pageSize` 

    Default value is `100`

* `dialect` 

    Possible values are:

    * `ckan`
    * `dkan`

    Default is `ckan`

* `filters` 

    A map of key value pairs to filter datasets

* `pulse` 
    
    Ticker in milliseconds for emitting datasets into the pipe. Default is no ticker.

## Data Info Object

* `total` 

    Total number of datasets

* `counter` 

    The number of this dataset

* `identifier` 
    
    The unique identifier in the source of this dataset

## Build
Requirements:
 * Git
 * Maven
 * Java

```bash
$ git clone https://gitlab.com/european-data-portal/harvester/importing-ckan.git
$ cd importing-ckan
$ mvn package
```

## Run

```bash
$ java -jar target/importing-ckan-far.jar
```

## Docker

Build docker image:

```bash
$ docker build -t importing-ckan .
```

Run docker image:

```^bash
$ docker run -it -p 8080:8080 importing-ckan
```

## License

[Apache License, Version 2.0](LICENSE.md)
